package se.excosoft.integration.company.data;

import java.util.Map;

import jersey.repackaged.com.google.common.collect.Maps;

public class DataProvider {

	private static Map<String,String> mapData = Maps.newHashMap();
	static {
		for (int i = 0; i < 10; i++) {
			mapData.put("thing"+i, "value"+i);
		}
	}

	
	public static Map<String, String> getMapData() {
		return mapData;
	}
	
	public static void set(String key, String value) {
		mapData.put(key, value);
	}
	
	public static String get(String key) {
		return mapData.get(key);
	}

	public static void remove(String thing) {
		mapData.remove(thing);
		
	}
}
