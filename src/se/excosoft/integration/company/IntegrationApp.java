/**
 * Copyright (c) 2008-2012 Excosoft AB.
 */

package se.excosoft.integration.company;

import org.glassfish.jersey.filter.LoggingFilter;
import org.glassfish.jersey.server.ResourceConfig;

import se.excosoft.integration.common.filter.SessionAuthFilter;
import se.excosoft.integration.common.filter.SessionExitFilter;

public class IntegrationApp extends ResourceConfig {
	
	public IntegrationApp() {
		this.packages(
				// Job resource
				//"se.excosoft.integration.job.resource",
				// Domain resource
				//"se.excosoft.integration.domain.resource",
				// Workspace resource
				//"se.excosoft.integration.workspace.resource",
				// Publication resource
				//"se.excosoft.integration.publication.resource",
				// File resource
				//"se.excosoft.integration.file.resource",
				// Configurator resource
				//"se.excosoft.integration.configurator.resource",
				
				// Translation resource
				//"se.excosoft.integration.translation.resource",
				// Translation machine resource
				//"se.excosoft.integration.translation.machine.resource",
				// Custom providers
				//"se.excosoft.integration.job.custom.provider",
				//Thing provider
				"se.excosoft.integration.company.provider",
				// Common providers
				"se.excosoft.integration.common.provider",
				// Custom resources
				//"se.excosoft.integration.custom.resource"
				//Company resources
				"se.excosoft.integration.company.thing"
				
				);
				
		register(LoggingFilter.class);
		register(SessionExitFilter.class);
		register(SessionAuthFilter.class);
	}
}
