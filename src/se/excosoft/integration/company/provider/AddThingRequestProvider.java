package se.excosoft.integration.company.provider;

import javax.ws.rs.Consumes;

import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.ext.Provider;

import se.excosoft.integration.common.provider.GsonProvider;
import se.excosoft.integration.company.thing.request.AddThingRequest;


@Provider
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class AddThingRequestProvider extends GsonProvider<AddThingRequest> {

	public AddThingRequestProvider() {
		super(AddThingRequest.class);
	}

}
