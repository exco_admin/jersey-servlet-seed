package se.excosoft.integration.company.thing.job;

import se.excosoft.integration.common.job.BaseJob;
import se.excosoft.integration.common.job.exception.JobException;
import se.excosoft.integration.company.data.DataProvider;
import se.excosoft.integration.company.thing.request.GetThingRequest;
import se.excosoft.integration.company.thing.response.GetThingsResponse;

public class GetThingsJob extends BaseJob<GetThingsResponse> {

	public GetThingsJob(GetThingRequest request) {
		response = new GetThingsResponse();
	}

	@Override
	public void validate() throws JobException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void execute() throws JobException {
		response.setThings(DataProvider.getMapData());
		
	}

}
