package se.excosoft.integration.company.thing.job;

import org.apache.commons.lang3.StringUtils;

import se.excosoft.integration.common.job.BaseJob;
import se.excosoft.integration.common.job.exception.JobBadDataException;
import se.excosoft.integration.common.job.exception.JobException;
import se.excosoft.integration.company.data.DataProvider;
import se.excosoft.integration.company.thing.response.ThingJobResponse;

public class DeleteThingJob extends BaseJob<ThingJobResponse> {

	
	
	private String thing;

	public DeleteThingJob(String thing) {
		this.thing = thing;
	}

	@Override
	public void validate() throws JobException {
		if (StringUtils.isBlank(thing)) {
			throw new JobBadDataException("Invalid thing");
		}
	}

	@Override
	public void execute() throws JobException {
		DataProvider.remove(thing);
		
	}

}
