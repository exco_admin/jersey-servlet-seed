package se.excosoft.integration.company.thing.job;


import org.apache.commons.lang3.StringUtils;

import se.excosoft.integration.common.job.BaseJob;
import se.excosoft.integration.common.job.exception.JobBadDataException;
import se.excosoft.integration.common.job.exception.JobException;
import se.excosoft.integration.company.data.DataProvider;
import se.excosoft.integration.company.thing.response.ThingJobResponse;
import se.excosoft.integration.file.response.FileExistsResponse;
import se.skribenta.api.SFile;

public class GetThingJob extends BaseJob<ThingJobResponse> {
	
	private final String thing;
	
	
	public GetThingJob(String thing) {
		this.thing = thing;
		this.response = new ThingJobResponse();
	}
	
	@Override
	public void validate() throws JobException {
		if (StringUtils.isBlank(thing)) {
			throw new JobBadDataException("Invalid file path");
		}
	}
	
	@Override
	public void execute() throws JobException {
		try {
			
			response.setThing(thing+":"+DataProvider.get(thing));
			
		} catch (Throwable exc) {
			response.setFailure();
		}
	}
	
}
