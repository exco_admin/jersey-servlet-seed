
package se.excosoft.integration.company.thing.model;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.io.FilenameUtils;

import com.google.common.collect.Lists;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import se.excosoft.api.skribenta.resource.IResource;
import se.skribenta.api.SFile;
import se.skribenta.api.SLanguage;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ThingModel {
	
	@Expose
	@SerializedName("name")
	@XmlElement(
			name = "name")
	private final String name;
	
	@Expose
	@SerializedName("value")
	@XmlElement(
			name = "value")
	private final String value;
	
	@Expose
	@SerializedName("field2")
	@XmlElement(
			name = "field2")
	private int field2;
	
	@Expose
	@SerializedName("field3")
	@XmlElement(
			name = "field3")
	private float field3;
	
	@Expose
	@SerializedName("field4")
	@XmlElement(
			name = "field4")
	private List<String> field4;
	
	@Expose
	@SerializedName("field5")
	@XmlElement(
			name = "field5")
	private boolean field5;
	
	
	public ThingModel(String thing, String value) {
		this.name = thing;
		this.value = value;
	}
	
	
}
