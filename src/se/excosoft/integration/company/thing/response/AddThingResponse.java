package se.excosoft.integration.company.thing.response;

import javax.xml.bind.annotation.XmlElement;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import se.excosoft.integration.common.response.BaseResponse;
import se.excosoft.integration.company.thing.model.ThingModel;

public class AddThingResponse extends BaseResponse {
	@Expose
	@SerializedName("thing")
	@XmlElement(
			name = "thing")
	private ThingModel thing;

	public ThingModel getThing() {
		return thing;
	}

	public void setThing(ThingModel thing) {
		this.thing = thing;
	}
	
	
	
}
