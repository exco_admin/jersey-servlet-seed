
package se.excosoft.integration.company.thing.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import se.excosoft.integration.common.model.FileModel;
import se.excosoft.integration.common.response.BaseResponse;
import se.excosoft.integration.company.thing.model.ThingModel;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GetThingResponse extends BaseResponse {
	
	@Expose
	@SerializedName("thing")
	@XmlElement(
			name = "thing")
	private ThingModel thing;
	
	public GetThingResponse() {
		this(null);
	}
	
	public GetThingResponse(ThingModel thing) {
		this.thing = thing;
	}
	
	
}
