package se.excosoft.integration.company.thing.response;

import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import se.excosoft.integration.common.response.BaseResponse;
import se.excosoft.integration.company.thing.model.ThingModel;
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GetThingsResponse extends BaseResponse {
	
		
		@Expose
		@SerializedName("things")
		@XmlElement(
				name = "things")
		private Map<String,String> things;
		
		public GetThingsResponse() {
			super();
		}

		public void setThings(Map<String, String> things) {
			this.things = things;
		}
		

}
