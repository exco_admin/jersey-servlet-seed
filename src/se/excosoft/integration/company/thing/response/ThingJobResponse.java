
package se.excosoft.integration.company.thing.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import se.excosoft.integration.common.response.BaseResponse;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ThingJobResponse extends BaseResponse {
	
	@Expose
	@SerializedName("thing")
	@XmlElement(
			name = "thing")
	private String thing;

	public String getThing() {
		return thing;
	}

	public void setThing(String thing) {
		this.thing = thing;
	}
	
	
}
