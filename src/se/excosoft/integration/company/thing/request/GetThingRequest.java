
package se.excosoft.integration.company.thing.request;

import javax.ws.rs.QueryParam;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import se.excosoft.integration.common.request.BaseRequest;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GetThingRequest extends BaseRequest {
	
	@Expose
	@SerializedName("thing1")
	@XmlElement(
			name = "thing1")
	@QueryParam("thing1")
	private Boolean thing1;
	
	@Expose
	@SerializedName("thing2")
	@XmlElement(
			name = "thing2")
	@QueryParam("thing2")
	private Integer thing2;
	
	public GetThingRequest() {
	}
	
}
