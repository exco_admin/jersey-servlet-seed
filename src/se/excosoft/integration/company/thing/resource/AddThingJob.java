package se.excosoft.integration.company.thing.resource;

import se.excosoft.integration.common.job.BaseJob;
import se.excosoft.integration.common.job.exception.JobException;
import se.excosoft.integration.company.data.DataProvider;
import se.excosoft.integration.company.thing.model.ThingModel;
import se.excosoft.integration.company.thing.response.AddThingResponse;

public class AddThingJob extends BaseJob<AddThingResponse> {

	private String thing;
	private String value;

	public AddThingJob(String thing, String value) {
		this.thing = thing;
		this.value = value;
		response = new AddThingResponse();
	}

	@Override
	public void validate() throws JobException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void execute() throws JobException {
		DataProvider.set(thing, value);
		response.setThing(new ThingModel(thing, value));
	}

}
