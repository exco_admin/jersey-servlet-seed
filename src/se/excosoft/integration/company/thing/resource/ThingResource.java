
package se.excosoft.integration.company.thing.resource;

import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import se.excosoft.integration.common.resource.BaseResource;
import se.excosoft.integration.company.thing.job.GetThingsJob;
import se.excosoft.integration.company.thing.job.DeleteThingJob;
import se.excosoft.integration.company.thing.job.GetThingJob;
import se.excosoft.integration.company.thing.request.AddThingRequest;
import se.excosoft.integration.company.thing.request.GetThingRequest;

@Path("domains/{domain}/workspaces/{workspace}/custom/things")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
public class ThingResource extends BaseResource {
	
	@PathParam("domain")
	private String domain;
	
	@PathParam("workspace")
	private String workspace;
	
	@QueryParam("async")
	private Boolean asynchronous;
	
	@GET
	public Response getThings(@BeanParam GetThingRequest request) {
		return execute(() -> new GetThingsJob(request));
	}
	
	@GET
	@Path("item/{thing}")
	public Response getThingItem(@PathParam("thing") String thing) {
		return execute(() -> new GetThingJob(thing));
	}
	
	@GET
	@Path("{thing}")
	public Response getThing(@PathParam("thing") String thing) {
		return execute(() -> new GetThingJob(thing));
	}
	
	@PUT
	@Path("{thing}")
	public Response addThing(@PathParam("thing") String thing, AddThingRequest request) {
		return execute(() -> new AddThingJob(thing,request.getValue()));
	}
	
	@POST
	@Path("{thing}")
	public Response updateThing(@PathParam("thing") String thing) {
		return execute(() -> new GetThingJob(thing));
	}
	
	@DELETE
	@Path("{thing}")
	public Response checkFileExists(@PathParam("thing") String thing) {
		return execute(() -> new DeleteThingJob(thing));
	}
	
	
	
	@Override
	protected String getDomain() {
		return domain;
	}
	
	@Override
	protected String getWorkspace() {
		return workspace;
	}
	
	@Override
	protected boolean isAsynchronous() {
		return asynchronous != null && asynchronous.booleanValue();
	}
}
