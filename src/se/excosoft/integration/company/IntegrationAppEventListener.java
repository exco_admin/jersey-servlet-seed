/**
 * Copyright (c) 2008-2012 Excosoft AB.
 */

package se.excosoft.integration.company;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import se.excosoft.util.logging.LoggingUtil;
import se.excosoft.utils.concurrent.TaskThreadPool;

@WebListener
public class IntegrationAppEventListener implements ServletContextListener {
	
	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		TaskThreadPool.shutdownAndWait(60);
	}
	
	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		TaskThreadPool.init();
		LoggingUtil.setupLogging("Integration");
	}
}
